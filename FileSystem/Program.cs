﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;
using System.Threading;
using ClassLibrary1.EventArgs;

namespace FileSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = "C:/";
            FileSystemVisitor fileVisitor = new FileSystemVisitor(path, (x) => { if (x.Extension == ".sys") return false; return true; }, null);
            fileVisitor.StartExecuting += WriteStartMessage;
            fileVisitor.FinishExecuting += DoFinishAction;
            fileVisitor.FileFinded += HandleFile;
            fileVisitor.FilteredFileFinded += HandleFilteredFile;

            foreach (var item in fileVisitor.GetAllFoldersAndFiles())
            {
                Console.WriteLine(item.FullName + " " + item.Extension);
                Thread.Sleep(10);
            }

            Console.ReadKey();
        }

        static void WriteStartMessage(object sender, EventArgs e)
        {
            Console.WriteLine("Execution of scanning begin!");
        }

        static void DoFinishAction(object sender, EventArgs e)
        {
            Console.WriteLine("Execution of scanning finish!");
        }

        static void HandleFile(object sender, FileEventArgs e)
        {
            if (e.File.Extension == ".log" || e.File.Extension == ".sys")
            {
                Console.WriteLine("This file send to your repository");
            }
        }

        static void HandleFilteredFile(object sender, FileEventArgs e)
        {
            if (e.File.Extension == ".log" || e.File.Extension == ".sys")
            {
                Console.WriteLine("This file deleteg from your repository");
            }
        }
    }
}
