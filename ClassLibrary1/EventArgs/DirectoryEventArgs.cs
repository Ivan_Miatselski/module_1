﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1.EventArgs
{
    public class DirectoryEventArgs : CommonEventArgs
    {
        public DirectoryInfo Directory { get; set; }
    }
}
