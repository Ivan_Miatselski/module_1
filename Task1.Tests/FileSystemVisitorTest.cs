﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using NUnit.Framework;
using ClassLibrary1;
using Moq;
using ClassLibrary1.EventArgs;

namespace Task1.Tests
{
    [TestFixture]
    public class FileSystemVisitorTest
    {
        private void EventFileFound(object sender, FileEventArgs args)
        {
            args.FlagToRun = false;
        }

        private void EventFilteredFileFound(object sender, FileEventArgs args)
        {
            args.FlagToRun = true;
        }

        private void EventDirectoryFound(object sender, DirectoryEventArgs args)
        {
            args.FlagToRun = false;
        }

        private void EventFilterDirectoryFound(object sender, DirectoryEventArgs args)
        {
            args.FlagToRun = true;
        }

        [TestCase("")]
        [TestCase(null)]
        public void FileSystemVisitor_NullOrEmptyParameter_ArgumentException(string path)
        {
            Assert.Throws(typeof(ArgumentException), () => new FileSystemVisitor(path));
        }

        [TestCase("C:/", null, null)]
        public void FileSystemVisitor_NullDelegates_ArgumentNullException(string path, Func<FileInfo, bool> filterFiles, Func<DirectoryInfo, bool> filterDirectories)
        {
            Assert.Throws(typeof(ArgumentNullException), () => new FileSystemVisitor(path, filterFiles, filterDirectories));
        }

        [TestCase("C:/")]
        public void GetAllFoldersAndFiles_SetEventFileFinded_ChangeProperty(string path)
        {
            var fileVisitor = new FileSystemVisitor(path);
            fileVisitor.FileFinded += EventFileFound;
            int index = 0;
            foreach (var item in fileVisitor.GetAllFoldersAndFiles())
            {
                index++;
            }
            Assert.AreEqual(1, index);
        }

        [TestCase("C:/")]
        public void GetAllFoldersAndFiles_SetEventFileFindedAndFilteredFileFinded_ChangeProperty(string path)
        {
            var fileVisitor = new FileSystemVisitor(path);
            fileVisitor.FileFinded += EventFileFound;
            fileVisitor.FilteredFileFinded += EventFilteredFileFound;
            int index = 0;
            foreach (var item in fileVisitor.GetAllFoldersAndFiles())
            {
                index++;
                if (index == 30)
                {
                    break;
                }
            }
            Assert.AreNotEqual(1, index);
        }

        [TestCase("C:/")]
        public void GetAllFoldersAndFiles_SetEventFileFindedAndFilteredFileFindedAndFilterFile_ChangeProperty(string path)
        {
            var fileVisitor = new FileSystemVisitor(path, (x) =>
            {
                if (x.Name.Length != 0)
                {
                    return false;
                }
                return true;
            }, null);
            fileVisitor.FileFinded += EventFileFound;
            fileVisitor.FilteredFileFinded += EventFilteredFileFound;
            int index = 0;
            foreach (var item in fileVisitor.GetAllFoldersAndFiles())
            {
                index++;
            }
            Assert.AreEqual(1, index);
        }

        [TestCase("C:/")]
        public void GetAllFoldersAndFiles_SetEventDirectoryFound_ChangeProperty(string path)
        {
            var fileVisitor = new FileSystemVisitor(path);
            fileVisitor.DirectoryFinded += EventDirectoryFound;
            int index = 0;
            foreach (var item in fileVisitor.GetAllFoldersAndFiles())
            {
                if (item is DirectoryInfo)
                {
                    index++;
                }
            }
            Assert.AreEqual(1, index);
        }

        [TestCase("C:/")]
        public void GetAllFoldersAndFiles_SetEventDirectoryFoundAndFilterDirectoryFound_ChangeProperty(string path)
        {
            var fileVisitor = new FileSystemVisitor(path);
            fileVisitor.DirectoryFinded += EventDirectoryFound;
            fileVisitor.FilteredDirectoryFinded += EventFilterDirectoryFound;
            int index = 0;
            foreach (var item in fileVisitor.GetAllFoldersAndFiles())
            {
                if (item is DirectoryInfo)
                {
                    index++;
                    if (index >= 30)
                    {
                        break;
                    }
                }
            }
            Assert.AreNotEqual(1, index);
        }

        [TestCase("C:/")]
        public void GetAllFoldersAndFiles_SetEventDirectoryFoundAndFilterDirectoryFoundAndFilteredFunc_ChangeProperty(string path)
        {
            var fileVisitor = new FileSystemVisitor(path, null, (x) =>
            {
                if (x.LastWriteTime != DateTime.Now)
                {
                    return false;
                }
                return true;
            });
            fileVisitor.DirectoryFinded += EventDirectoryFound;
            fileVisitor.FilteredDirectoryFinded += EventFilterDirectoryFound;
            int index = 0;
            foreach (var item in fileVisitor.GetAllFoldersAndFiles())
            {
                if (item is DirectoryInfo)
                {
                    index++;
                }
            }
            Assert.AreEqual(1, index);
        }
    }
}
